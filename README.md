# Les Gaulois - CI/CD Gitlab - Java Spring Boot - B3C2

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

### Membres du projet

- FIGUEIREDO Adrien
- MARIGNIER Vincent
- MARTIN Gabriel
- TOURRET Mathilde

(Simon Fakoury n'est pas avec nous, nous ne savons pas pourquoi il est dans notre groupe MyLearningBox)

## CI/CD pipeline

La pipeline possède 2 jobs :

1. Run tests
2. Build et export

### Fonctionnement

1. Test

```yml
script:
  - mvn clean test
  - mvn clean test -Pintegration-testing
```

2. Build et export du jar

```yml
script:
  - mvn -q package jmeter:configure -Dmaven.test.skip=true
artifacts:
  paths:
    - build/*.jar
  expire_in: 1 week
```

L'artifacts nous permet de mettre à disposition le fichier build via gitlab pour pouvoir récupérer le .jar. Celui ci est supprimé au bout d'une semaine.

### Features du projet

- Ecran de connexion
- Enregistrement utilisateur
- Endpoint d'authentification

### Screen
Le build avec les 2 jobs:
![build](assets/pipeline.png)
L'export:
![export du jar](assets/exportJar.png)
