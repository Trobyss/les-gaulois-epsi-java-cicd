FROM maven:3.3.9-jdk-8

COPY . /usr/src/app
WORKDIR /usr/src/app

RUN mvn -q package jmeter:configure -Dmaven.test.skip=true
