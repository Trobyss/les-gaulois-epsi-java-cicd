package com.example.springpipelinedemo.dto.mapper;

import com.example.springpipelinedemo.dto.UserDto;
import com.example.springpipelinedemo.model.User;
import org.springframework.stereotype.Component;
 
@Component
public class UserMapper implements Mapper<User, UserDto> {

    @Override
    public UserDto map(User user) {
        return new UserDto(user.getUsername());
    }

}
